import requests
import urllib3
from bs4 import BeautifulSoup
from wbut.models import Result, BasicDetails, College, SemesterOverview, TimeTable
import urllib3
from io import BytesIO
import logging
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logger = logging.getLogger(__name__)


# This Function downloads the result from makaut website
def downloadResult(usn,sem,cookie,token):
    headers = {
        'X-CSRF-TOKEN' : token,
        "Cookie": cookie
    }
    url = "https://makaut1.ucanapply.com/smartexam/public/download-pdf-result"
    semester = "SM0" + sem
    downloadData = {
        "_token": token,
        "ROLLNO": usn,
        "SEMCODE": semester,
        "examtype": "result-details",
        "downloadFileName": usn + "_marksheet"
    }
    r = requests.post(url, data=downloadData, headers=headers, verify=False)
    f = BytesIO(r.content)
    return f


def parseResult(usn, sem, cookie, token):
    headers = {
        "X-CSRF-TOKEN": token,
        "Cookie": cookie
    }
    semester = "SM0" + sem
    data = {
        "_token": token,
        "ROLLNO": usn,
        "SEMCODE": semester,
        "examtype": "result-details"
    }

    url = "https://makaut1.ucanapply.com/smartexam/public//get-result-details"
    r = requests.post(url, data=data, headers=headers, verify=False)

    try:
        data = r.json()["html"]
        soup = BeautifulSoup(data, 'html5lib')
        table = soup.findAll('table')
        result = table[3]
        basicdetails = table[1]
        detailsrow = basicdetails.findAll("tr")
        basic_details = detailsrow[1].findAll('td')
        name = basic_details[0].find('span').get_text()
        roll_no = basic_details[1].find('span').get_text()
        reg_no = detailsrow[2].find('td').find('span').get_text()
        result_details_table = table[5]
        if int(sem) % 2 == 0:
            sgpa = result_details_table.findAll('tr')[1].find('td').find('strong').get_text()
            sgpa = sgpa[-6:]
            ygpa = result_details_table.findAll('tr')[2].find('td').find('strong').get_text()
            ygpa = ygpa[-4:]
            college_name = result_details_table.findAll('tr')[4].find('td').get_text()
            college_name = college_name[24:]
            status = result_details_table.findAll('tr')[3].find('td').get_text()
            status = status[-1:]
        else:
            sgpa = result_details_table.findAll('tr')[0].find('td').find('strong').get_text()
            sgpa = sgpa[-4:]
            ygpa = ""
            college_name = result_details_table.findAll('tr')[4].find('td').get_text()
            college_name = college_name[24:]
            status = result_details_table.findAll('tr')[3].find('td').get_text()
            status = status[-1:]

        # print(basic_details)
        get_details = BasicDetails.objects.filter(roll_no=roll_no)
        if get_details.count() <= 0:
            details_to_save = BasicDetails(name=name, reg_no=reg_no, roll_no=roll_no, college=college_name)
            details_to_save.save()
            semester_overview = SemesterOverview(basicDetails=details_to_save, semester=sem, ygpa=ygpa, cgpa=sgpa, status=status)
            semester_overview.save()
        else:
            get_details = get_details.first()
            semester_overview = SemesterOverview(basicDetails=get_details, semester=sem, ygpa=ygpa, cgpa=sgpa, status=status)
            semester_overview.save()
        rows = result.findAll("tr")
        get_details = BasicDetails.objects.filter(roll_no=roll_no).first()
        for i in range(1,len(rows)-1):
            cell = rows[i].findAll('td')
            subject_code = cell[0].get_text()
            subject_name = cell[1].get_text()
            letter_grade = cell[2].get_text()
            points = cell[3].get_text()
            credit = cell[4].get_text()
            credit_points = cell[5].get_text()
            result_to_save = Result(basicDetails = get_details,subject_code=subject_code, subject_name=subject_name,letter_grade=letter_grade,points=points,credit=credit,credit_points=credit_points,semester=semester_overview.semester)
            result_to_save.save()
    except Exception as ex:
        print(ex)


def parseIndexPage():
    try:
        url = "https://makaut1.ucanapply.com/smartexam/public/result-details"
        s = requests.session()
        r = s.get(url, verify=False, timeout=2)
        xsrf_token = 'XSRF-TOKEN=' + r.cookies['XSRF-TOKEN'] + ';'
        makaut_cookie = 'maulana_abul_kalam_azad_university_of_technology_session=' + r.cookies[
            'maulana_abul_kalam_azad_university_of_technology_session']
        cookie = xsrf_token + makaut_cookie
        soup = BeautifulSoup(r.content,'html5lib')
        token = soup.find('meta', {'name':'csrf-token'})['content']
        return cookie, token
    except Exception as e:
        logger.info(e)
        print(e)
        raise TimeoutError


def parseCollegeList():
    url = "https://makautwb.ac.in/page.php?id=363"
    r = requests.get(url, verify=False)
    soup = BeautifulSoup(r.content, 'html5lib')
    table = soup.findAll('table')
    collegeList = table[-1].findAll('tr')
    for i in range(1, len(collegeList)):
        collegeDetails = collegeList[i].findAll('td')
        collegeName = collegeDetails[2].get_text().replace('\n', '').replace('\t', '')
        collegeCode = collegeDetails[1].get_text().replace('\n', '').replace('\t', '')
        data = College(collegeCode=int(collegeCode), collegeName=collegeName)
        data.save()


def parseimeTable(cookie,token,stream, semester):
    headers = {
        "X-CSRF-TOKEN": token,
        "Cookie": cookie
    }
    url = "https://makaut1.ucanapply.com/smartexam/public//get-routine-details"
    data = {
        "_token":token,
        "STREAM":stream,
        "SEMCODE":semester
    }
    r = requests.post(url, data=data, headers=headers,verify=False)
    try:
        data = r.json()["html"]
        soup = BeautifulSoup(data, 'html5lib')
        table = soup.findAll('table')
        routine = table[3]
        rows = routine.findAll('tr')
        for i in range(1,len(rows)-1):
            cells = rows[i].findAll('td')
            date = cells[1].get_text()
            time = cells[2].get_text()
            subject_code = cells[4].get_text()
            subject_name = cells[5].get_text()
            details_to_save = TimeTable(date=date,time=time,sem=semester,subject_code=subject_code,subject_name=subject_name,scheme=stream)
            details_to_save.save()
    except Exception:
        raise Exception


def downloadTimeTableParse(stream, semester, cookie, token):
    headers = {
        "X-CSRF-TOKEN": token,
        "Cookie": cookie
    }
    data = {
        "_token" : token,
        "STREAM" : stream,
        "SEMCODE" : semester,
        "downloadFileName": stream
    }

    url = "https://makaut1.ucanapply.com/smartexam/public/download-pdf-routine-details"
    r = requests.post(url, data=data, headers=headers, verify=False)
    f = BytesIO(r.content)
    return f